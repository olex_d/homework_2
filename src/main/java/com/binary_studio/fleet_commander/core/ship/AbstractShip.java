package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;

public class AbstractShip {

	protected final String name;

	protected PositiveInteger shieldHP;

	protected PositiveInteger hullHP;

	protected PositiveInteger capacitor;

	protected final PositiveInteger capacitorRegeneration;

	protected final PositiveInteger powergridCapacity;

	protected final PositiveInteger speed;

	protected final PositiveInteger size;

	public AbstractShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger powergridCapacity, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.powergridCapacity = powergridCapacity;
		this.speed = speed;
		this.size = size;
	}

}
