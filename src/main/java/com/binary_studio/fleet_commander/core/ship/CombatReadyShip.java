package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip extends AbstractCombatShip implements CombatReadyVessel {

	private final PositiveInteger initialShieldHP;

	private final PositiveInteger initialHullHP;

	private final PositiveInteger initialCapacity;

	public CombatReadyShip(AbstractCombatShip dockedShip) {
		super(dockedShip.name, dockedShip.shieldHP, dockedShip.hullHP, dockedShip.capacitor,
				dockedShip.capacitorRegeneration, dockedShip.powergridCapacity, dockedShip.speed, dockedShip.size,
				dockedShip.attackSubsystem, dockedShip.defenciveSubsystem);

		this.initialShieldHP = dockedShip.shieldHP;
		this.initialHullHP = dockedShip.hullHP;
		this.initialCapacity = dockedShip.capacitor;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public void startTurn() {
		System.out.println(String.format("Ship %s starts move", this.name));
	}

	@Override
	public void endTurn() {
		rechargeCapacitor().ifPresentOrElse(result -> this.capacitor = result,
				() -> this.capacitor = this.initialCapacity);
		System.out.println(String.format("Ship %s finished move", this.name));
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		var attackCapacityRequired = attackSubsystem.getCapacitorConsumption().value();
		if (attackCapacityRequired > this.capacitor.value()) {
			return Optional.empty();
		}
		this.capacitor = PositiveInteger.of(this.capacitor.value() - attackCapacityRequired);

		var dealtDamage = attackSubsystem.attack(target);
		var performingAttack = new AttackAction(dealtDamage, this, target, attackSubsystem);
		return Optional.of(performingAttack);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		var reducedAttack = defenciveSubsystem.reduceDamage(attack);

		var shieldAfterDamage = this.shieldHP.value() - reducedAttack.damage.value();
		if (shieldAfterDamage > 0) { // shield covered damage
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() - reducedAttack.damage.value());
			return new AttackResult.DamageRecived(reducedAttack.weapon, reducedAttack.damage, reducedAttack.target);
		}
		this.shieldHP = PositiveInteger.of(0);

		var hullAfterDamage = this.hullHP.value() - Math.abs(shieldAfterDamage);
		if (hullAfterDamage > 0) { // hull covered damaged
			this.hullHP = PositiveInteger.of(this.hullHP.value() - reducedAttack.damage.value());
			return new AttackResult.DamageRecived(reducedAttack.weapon, reducedAttack.damage, reducedAttack.target);
		}

		return new AttackResult.Destroyed();
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		var regenerateCapacityRequired = defenciveSubsystem.getCapacitorConsumption().value();
		if (regenerateCapacityRequired > this.capacitor.value()) {
			return Optional.empty();
		}
		var defenceRegeneration = defenciveSubsystem.regenerate();

		var maxShieldRegen = defenceRegeneration.shieldHPRegenerated;
		var shieldRegenerated = regenerateShield(maxShieldRegen);

		var maxHullRegen = defenceRegeneration.hullHPRegenerated;
		var hullRegenerated = regenerateHull(maxHullRegen);

		this.capacitor = PositiveInteger.of(this.capacitor.value() - regenerateCapacityRequired);

		var newRegeneration = new RegenerateAction(shieldRegenerated, hullRegenerated);
		return Optional.of(newRegeneration);
	}

	private PositiveInteger regenerateShield(PositiveInteger maxShieldRegen) {
		var shieldHPLoss = this.initialShieldHP.value() - this.shieldHP.value();

		if (shieldHPLoss < maxShieldRegen.value()) {
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() + shieldHPLoss);
			return PositiveInteger.of(shieldHPLoss);
		}

		this.shieldHP = PositiveInteger.of(this.shieldHP.value() + maxShieldRegen.value());
		return maxShieldRegen;
	}

	private PositiveInteger regenerateHull(PositiveInteger maxHullRegen) {
		var hullHPLoss = this.initialHullHP.value() - this.hullHP.value();

		if (hullHPLoss < maxHullRegen.value()) {
			this.hullHP = PositiveInteger.of(this.hullHP.value() + hullHPLoss);
			return PositiveInteger.of(hullHPLoss);
		}

		this.hullHP = PositiveInteger.of(this.hullHP.value() + maxHullRegen.value());
		return maxHullRegen;
	}

	private Optional<PositiveInteger> rechargeCapacitor() {
		var rechargedCapacityVal = this.capacitor.value() + this.capacitorRegeneration.value();
		if (rechargedCapacityVal <= this.initialCapacity.value()) {
			var rechargedCapacity = PositiveInteger.of(rechargedCapacityVal);
			return Optional.of(rechargedCapacity);
		}
		return Optional.empty();
	}

}
