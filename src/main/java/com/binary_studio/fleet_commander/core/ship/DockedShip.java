package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip extends AbstractCombatShip implements ModularVessel {

	// new ship is not using modules
	private PositiveInteger usedPowergridPower = PositiveInteger.of(0);

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		super(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, powergridOutput, speed, size);
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			var attackRequiredPg = subsystem.getPowerGridConsumption().value();
			energizeSubsystem(attackRequiredPg);
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			var defenceRequiredPg = subsystem.getPowerGridConsumption().value();
			energizeSubsystem(defenceRequiredPg);
		}
		this.defenciveSubsystem = subsystem;
	}

	private void energizeSubsystem(int subsystemRequirement) throws InsufficientPowergridException {
		this.usedPowergridPower = PositiveInteger.of(this.usedPowergridPower.value() + subsystemRequirement);
		if (this.usedPowergridPower.value() > this.powergridCapacity.value()) {
			var lackOfPower = this.usedPowergridPower.value() - this.powergridCapacity.value();
			throw new InsufficientPowergridException(lackOfPower);
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (attackSubsystem == null && defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(this);
	}

}
