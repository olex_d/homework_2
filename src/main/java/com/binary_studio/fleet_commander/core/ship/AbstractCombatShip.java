package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public class AbstractCombatShip extends AbstractShip {

	protected AttackSubsystem attackSubsystem;

	protected DefenciveSubsystem defenciveSubsystem;

	public AbstractCombatShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger powergridCapacity, PositiveInteger speed,
			PositiveInteger size) {
		super(name, shieldHP, hullHP, capacitor, capacitorRegeneration, powergridCapacity, speed, size);
	}

	public AbstractCombatShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger powergridCapacity, PositiveInteger speed,
			PositiveInteger size, AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		super(name, shieldHP, hullHP, capacitor, capacitorRegeneration, powergridCapacity, speed, size);
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

}
