package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger baseDamage;

	private final PositiveInteger optimalSize;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger capacitorUsage;

	private final PositiveInteger pgRequirement;

	public AttackSubsystemImpl(String name, PositiveInteger baseDamage, PositiveInteger optimalSize,
			PositiveInteger optimalSpeed, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, baseDamage, optimalSize, optimalSpeed, capacitorConsumption,
				powergridRequirments);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		var targetSize = Double.valueOf(target.getSize().value());
		var optimalSizeVal = Double.valueOf(this.optimalSize.value());
		var sizeReductionModifier = (targetSize >= optimalSizeVal ? 1 : (targetSize / optimalSizeVal));

		var targetSpeed = Double.valueOf(target.getCurrentSpeed().value());
		var optimalSpeedVal = Double.valueOf(this.optimalSpeed.value());
		var speedReductionModifier = (targetSpeed <= optimalSpeedVal ? 1 : (optimalSpeedVal / (2 * targetSpeed)));

		var baseDamageVal = Double.valueOf(this.baseDamage.value());
		var damage = baseDamageVal * Math.min(sizeReductionModifier, speedReductionModifier);

		return PositiveInteger.of(Math.ceil(damage));
	}

}
