package com.binary_studio.uniq_in_sorted_stream;

import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		var uniqueFilter = new IsUniqueId();
		return stream.filter(row -> uniqueFilter.test(row.getPrimaryId()));
	}

	private static class IsUniqueId implements Predicate<Long> {

		private Long largestFounded = null;

		@Override
		public boolean test(Long inputLong) {
			if (isInputUnique(inputLong)) {
				this.largestFounded = inputLong;
				return true;
			}
			return false;
		}

		private boolean isInputUnique(Long input) {
			return input != null && (this.largestFounded == null || input > this.largestFounded);
		}

	}

}
