package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		int height = 0;

		// level representing queue
		Queue<Department> depsQueue = new LinkedList<>();
		depsQueue.add(rootDepartment);

		while (true) {
			// amount of departments on level
			int depsCount = depsQueue.size();
			if (depsCount == 0) {
				return height;
			}
			++height;

			// deque all departments of current level and
			// enqueue all departments of next level
			while (depsCount > 0) {
				var currentLevelDepartment = depsQueue.remove();
				var validChildrenDepartments = currentLevelDepartment.subDepartments.parallelStream()
						.filter(Objects::nonNull).collect(Collectors.toList());
				depsQueue.addAll(validChildrenDepartments);
				--depsCount;
			}
		}
	}

}
